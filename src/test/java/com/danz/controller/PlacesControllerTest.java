package com.danz.controller;

import com.danz.service.FoursquareService;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.Arrays;

import static org.mockito.BDDMockito.anyMap;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PlacesController.class)
public class PlacesControllerTest {

    private static final String TEST_ERROR = "Test Error";
    private static final String TEST_JSON_EXCEPTION = "Test Json Exception";
    private static final String MAPPING = "/venues/test";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FoursquareService foursquareService;

    @MockBean
    private RestTemplate restTemplate;

    @Test
    public void shouldReturnVenueItems() throws Exception {

        JSONObject item1 = new JSONObject();
        JSONObject item2 = new JSONObject();
        JSONObject foursquareResponse = buildFoursquareResponse(item1, item2);
        given(this.foursquareService.getTopRecommendedPLaces(anyString(), anyMap()))
                .willReturn(foursquareResponse);

        JSONObject response = new JSONObject();
        response.accumulate(JsonKeys.KEY_RESULTS, Arrays.asList(item1, item2));
        this.mockMvc.perform(get(MAPPING))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(response.toString()));
    }

    @Test
    public void shouldReturnErrorForRestClientException() throws Exception {

        JSONObject errorJson = buildFoursquareErrorResponse();
        given(this.foursquareService.getTopRecommendedPLaces(anyString(), anyMap()))
                .willThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST,
                        "Error",
                        errorJson.toString().getBytes(),
                        Charset.forName("UTF-8")));

        JSONObject result = new JSONObject();
        result.accumulate(JsonKeys.KEY_ERROR, TEST_ERROR);
        this.mockMvc.perform(get(MAPPING))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(result.toString()));
    }

    @Test
    public void shouldReturnErrorForGenericException() throws Exception {

        given(this.foursquareService.getTopRecommendedPLaces(anyString(), anyMap()))
                .willThrow(new JSONException(TEST_JSON_EXCEPTION));

        JSONObject result = new JSONObject();
        result.accumulate(JsonKeys.KEY_ERROR, TEST_JSON_EXCEPTION);
        this.mockMvc.perform(get(MAPPING))
                .andExpect(status().is(HttpStatus.INTERNAL_SERVER_ERROR.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(result.toString()));
    }

    private JSONObject buildFoursquareResponse(JSONObject... items) {
        JSONObject group = new JSONObject();
        group.accumulate(JsonKeys.FS_KEY_ITEMS, Arrays.asList(items));
        JSONObject response = new JSONObject();
        response.accumulate(JsonKeys.FS_KEY_GROUPS, Arrays.asList(group));
        JSONObject result = new JSONObject();
        result.accumulate(JsonKeys.FS_KEY_RESPONSE, response);
        return new JSONObject(result.toString());
    }

    private JSONObject buildFoursquareErrorResponse() {
        JSONObject message = new JSONObject();
        message.accumulate(JsonKeys.FS_KEY_ERROR_DETAIL, TEST_ERROR);
        JSONObject response = new JSONObject();
        response.accumulate(JsonKeys.FS_KEY_META, message);
        return new JSONObject(response.toString());
    }

}