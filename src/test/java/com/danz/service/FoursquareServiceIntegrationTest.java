package com.danz.service;

import com.danz.IntegrationTest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@Category(IntegrationTest.class)
public class FoursquareServiceIntegrationTest {

    @Bean
    public RestTemplate rest(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Autowired
    private FoursquareService service;

    @Test
    public void shouldReturnTopVenues() {

        Map<String, String> params = new HashMap<>();
        JSONObject result = service.getTopRecommendedPLaces("London", params);
        JSONArray items = getResultItems(result);
        assertThat(items.length()).isGreaterThan(0);
    }

    @Test
    public void shouldReturnTop2Venues() {

        Map<String, String> params = new HashMap<>();
        params.put("limit", "2");
        JSONObject result = service.getTopRecommendedPLaces("London", params);
        JSONArray items = getResultItems(result);
        assertThat(items.length()).isEqualTo(2);
    }

    @Test(expected = HttpClientErrorException.class)
    public void shouldThrowErrorWhenCallingUrlMissingParameters() {

        Map<String, String> params = new HashMap<>();
        service.getTopRecommendedPLaces("", params);
    }

    private JSONArray getResultItems(JSONObject result) {
        JSONObject responseJson = result.getJSONObject("response");
        assertThat(responseJson).isNotNull();
        JSONArray groups = responseJson.getJSONArray("groups");
        assertThat(groups).isNotNull();
        return ((JSONObject) groups.get(0)).getJSONArray("items");
    }
}