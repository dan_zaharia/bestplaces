package com.danz.service;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FoursquareServiceTest {

    private static final String TEST_ERROR_RESPONSE = "test json error response";
    private static final String TEST_JSON_RESPONSE = "{\"test\":\"test json response\"}";
    private static final String TEST_PLACE = "TestPlace";

    @Mock
    private RestTemplate restTemplate;

    private FoursquareService service;

    @Before
    public void setup() {
        service = new FoursquareService(restTemplate);
    }

    @Test
    public void returnFoursquareResponse() {

        Map<String, String> params = new HashMap<>();
        when(restTemplate.getForObject(any(URI.class), any()))
                .thenReturn(TEST_JSON_RESPONSE);
        JSONObject result = service.getTopRecommendedPLaces(TEST_PLACE, params);

        assertThat(result).isNotNull();
        assertThat(result.toString()).isEqualTo(TEST_JSON_RESPONSE);
    }

    @Test(expected = HttpClientErrorException.class)
    public void returnFoursquareErrorResponse() {

        Map<String, String> params = new HashMap<>();
        when(restTemplate.getForObject(any(URI.class), any()))
                .thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST, TEST_ERROR_RESPONSE));
        service.getTopRecommendedPLaces(TEST_PLACE, params);
    }
}
