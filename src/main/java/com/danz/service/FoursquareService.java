package com.danz.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Map;

@Service
public class FoursquareService {

    public static final String CLIENT_ID = "client_id";
    public static final String CLIENT_SECRET = "client_secret";
    public static final String VERSION = "v";
    private static final String NEAR = "near";

    @Value("${foursquare.client.id}")
    private String clientId;

    @Value("${foursquare.client.secret}")
    private String clientSecret;

    @Value("${foursquare.version}")
    private String version;

    @Value("${foursquare.scheme}")
    private String scheme;

    @Value("${foursquare.host}")
    private String host;

    @Value("${foursquare.top.venues.path}")
    private String path;

    private final RestTemplate restTemplate;

    public FoursquareService(RestTemplate rest) {
        this.restTemplate = rest;
    }

    public JSONObject getTopRecommendedPLaces(String name, Map<String, String> allRequestParams) {

        UriComponents uriComponents = buildUriComponents(name, allRequestParams);
        URI uri = URI.create(uriComponents.toString());
        String foursquareResponse = restTemplate.getForObject(uri, String.class);
        return new JSONObject(foursquareResponse);
    }

    private UriComponents buildUriComponents(String name, Map<String, String> allRequestParams) {
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.newInstance()
                .scheme(scheme)
                .host(host)
                .path(path)
                .queryParam(CLIENT_ID, clientId)
                .queryParam(CLIENT_SECRET, clientSecret)
                .queryParam(VERSION, version);
        allRequestParams.forEach(uriComponentsBuilder::queryParam);
        uriComponentsBuilder.queryParam(NEAR, name);
        return uriComponentsBuilder.build();
    }
}
