package com.danz.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource(value= {"classpath:application.properties"})
public class FoursquareConfig {

    @Value("${foursquare.client.id}")
    private String clientId;

    @Value("${foursquare.client.secret}")
    private String clientSecret;

    @Value("${foursquare.version}")
    private String version;

    @Value("${foursquare.scheme}")
    private String scheme;

    @Value("${foursquare.host}")
    private String host;

    @Value("${foursquare.top.venues.path}")
    private String path;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfig() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
