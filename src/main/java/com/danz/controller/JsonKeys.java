package com.danz.controller;

public class JsonKeys {
    static final String FS_KEY_ERROR = "error";
    static final String FS_KEY_GROUPS = "groups";
    static final String FS_KEY_RESPONSE = "response";
    static final String FS_KEY_ITEMS = "items";
    static final String KEY_RESULTS = "results";
    static final String KEY_ERROR = "error";
    static final String FS_KEY_META = "meta";
    static final String FS_KEY_ERROR_DETAIL = "errorDetail";
}
