package com.danz.controller;

import com.danz.service.FoursquareService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.util.Map;

@RestController
public class PlacesController {

    private static final Logger logger = LoggerFactory.getLogger(PlacesController.class);
    public static final String MAPPING = "/venues/{name}";

    @Autowired
    private FoursquareService foursquareService;

    @GetMapping(value = MAPPING, produces = "application/json")
    @ResponseBody
    public ResponseEntity findPlaces(@PathVariable String name,
                                     @RequestParam Map<String,String> allRequestParams)
            throws Exception {

        JSONObject result = new JSONObject();
        JSONObject foursquareResult = foursquareService.getTopRecommendedPLaces(name, allRequestParams);
        if (foursquareResult.has(JsonKeys.FS_KEY_RESPONSE)) {
            getResultItems(result, foursquareResult);
            return ResponseEntity
                    .ok()
                    .body(result.toString());
        }
        throw new Exception(foursquareResult.getString(JsonKeys.FS_KEY_ERROR));
    }

    @ExceptionHandler
    public ResponseEntity resolveError(Exception e) {

        logger.error(e.getMessage(), e);
        JSONObject error = new JSONObject();
        error.accumulate(JsonKeys.KEY_ERROR, e.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(error.toString());
    }

    @ExceptionHandler
    public ResponseEntity resolveClientError(HttpClientErrorException e) {

        String fullMessage = e.getResponseBodyAsString();
        logger.error(fullMessage, e);
        JSONObject error = buildClientErrorResponse(fullMessage);
        return ResponseEntity.badRequest()
                .body(error.toString());
    }

    private void getResultItems(JSONObject result, JSONObject foursquareResult) {
        JSONObject response = foursquareResult.getJSONObject(JsonKeys.FS_KEY_RESPONSE);
        JSONArray groups = response.getJSONArray(JsonKeys.FS_KEY_GROUPS);
        JSONArray items = ((JSONObject) groups.get(0)).getJSONArray(JsonKeys.FS_KEY_ITEMS);
        result.accumulate(JsonKeys.KEY_RESULTS, items.toList());
    }

    private JSONObject buildClientErrorResponse(String fullMessage) {
        String errorMessage = new JSONObject(fullMessage)
                .getJSONObject(JsonKeys.FS_KEY_META)
                .getString(JsonKeys.FS_KEY_ERROR_DETAIL);
        JSONObject error = new JSONObject();
        error.accumulate(JsonKeys.KEY_ERROR, errorMessage);
        return error;
    }

}
