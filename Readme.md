# Best Places

API that integrates with the Foursquare API and allows you to search for a place by name and return the recommended or popular venues near that location.


### Prerequisites

* Java8
* maven


### Installing

Building the war

```
mvn clean package
```


## Running the tests

Run unit tests

```
mvn clean test
```

Run integration and unit tests

```
mvn clean install
```


## Deployment

Standard deployment to a servlet container (e.g. Tomcat)


## Approach

Chose maven as build tool to build the code and fetch dependencies and spring boot for minimizing configurations.

The single API endpoint is GET /venues/{location}

```
http://localhost:8080/bestPlaces/venues/london
```

Also it accepts all Foursquare parameters from the integrated endpoint: https://developer.foursquare.com/docs/venues/explore

```
http://localhost:8080/bestPlaces/venues/london?limit=2
```